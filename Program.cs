﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EFGETSTARTED
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using (var db = new BloggingContext())
            {
                // Create
                Console.WriteLine("Inserting a new blog");
                await db.AddAsync(new Blog { Url = "http://blogs.msdn.com/adonet" });
                await db.AddAsync(new Blog { Url = "http://devblogs.microsft.com/dotnet" });
                await db.SaveChangesAsync();

                // Read All
                Console.WriteLine("Querying for all blogs");
                var blogs = db.Blogs.OrderBy(b => b.BlogId);
                blogs.ToList().ForEach(b => Console.WriteLine($"{b.BlogId} - {b.Url}"));
                
                //Read One
                Console.WriteLine("Querying for a blog");
                var blog = await db.Blogs.FindAsync(2);
                Console.WriteLine($"{blog.BlogId} - {blog.Url}");

                // Update
                Console.WriteLine("Adding a post");              
                blog.Posts.Add(new Post{Title = "Hello World",Content = "I wrote an app using EF Core!"});
                await db.SaveChangesAsync();

                //Read All Blogs and Posts
                Console.WriteLine("Querying EAGER for all blogs and posts");
                var blogs1 = db.Blogs.Include(b => b.Posts).OrderBy(b => b.BlogId);
                blogs1.ToList().ForEach(b => 
                {
                    Console.WriteLine($"{b.BlogId} - {b.Url}");
                    b.Posts.ForEach(p => Console.WriteLine($"{p.Title}"));
                });

                Console.WriteLine("Querying EXPLICIT for all blogs and posts");
                var blogs2 = db.Blogs.OrderBy(b => b.BlogId);
                blogs2.ToList().ForEach(async b => 
                {
                    Console.WriteLine($"{b.BlogId} - {b.Url}");
                    await db.Entry(b).Collection(oblog => oblog.Posts).LoadAsync();
                    b.Posts.ForEach(p => Console.WriteLine($"{p.Title}"));
                });

                // Delete
                Console.WriteLine("Delete the blog");
                db.Remove(blog);
                await db.SaveChangesAsync();
            }
        }
    }
}
